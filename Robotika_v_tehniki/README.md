---
title: "Robotika v tehniki"
subtitle: "Delovni zvezek za osnovnošolski predmet"
author: "dr. David Rihtaršič"
date: 2019-09-06 14:47
classoption: onecolumn 			# [onecolumn, twocolumn]
titlepage: true
fontsize: 12pt
#geometry: "top=1cm, bottom=1cm, left=1cm, right=1cm"
numbersections: true
documentclass: article 		# [article, book, report]
csl: ieee.csl
#bibliography: bibtex.bib
lang: sl 				# [sl, en-US, us-GB]
template: skripta			# [eisvogel, skripta, raw] 
listings: true

eqnPrefix: ""
figPrefix: ""
secPrefix: "pogl."
codeBlockCaptions: true
lolTitle: "Kazalo Nalog:"

---
\newpage
\tableofcontents
\newpage

# Robotika v tehniki

